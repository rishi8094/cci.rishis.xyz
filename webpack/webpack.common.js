const Path = require('path');
const path = require('path');
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const srcFolder = Path.resolve(__dirname, '../src/pages/');
const fs = require('fs');


const walkSync = (dir = srcFolder, filelist = []) => {
  fs.readdirSync(dir).forEach(file => {
    filelist = fs.statSync(path.join(dir, file)).isDirectory() ?
      walkSync(path.join(dir, file), filelist) :
      filelist.concat(path.join(dir, file));

  });
  return filelist;
}

function getAllFiles() {
  let htmlPlugins = [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([{
      from: Path.resolve(__dirname, '../public'),
      to: 'public'
    }]),
    new HtmlWebpackPlugin({
      template: Path.resolve(__dirname, '../src/index.html')
    })
  ];
  let files = walkSync();
  for (var i = 0; i < files.length; i++) {
    let file = files[i].split("/");
    let cursor = file.indexOf("pages");
    htmlPlugins.push(new HtmlWebpackPlugin({
      filename: `${file.splice(cursor).join("/")}`,
      template: files[i]
    }));
  }
  return htmlPlugins;
}

module.exports = {
  entry: {
    app: Path.resolve(__dirname, '../src/scripts/index.js')
  },
  output: {
    path: Path.join(__dirname, '../build'),
    filename: 'js/[name].js'
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: false
    }
  },
  plugins: getAllFiles(),
  resolve: {
    alias: {
      '~': Path.resolve(__dirname, '../src')
    }
  },
  module: {
    rules: [{
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto'
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }
      },
    ]
  }
};
