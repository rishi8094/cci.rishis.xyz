import '../styles/normalize.scss';
import '../styles/index.scss';
import '../styles/projects.scss';
import '../styles/app.scss';
import '../styles/app/recaptcha.scss';
import '../styles/gist.scss';
import '../styles/terminal.scss';
import '../styles/whitepaper.scss';

if (!window.location.href.includes("iframe")) {
  const msg = `%c
              %@@@@@@@@@@@        CCI Portfolio
             &@@@@##@@@@@@/       written by
            %@@@@*  %@@@@/        @rish1s (Twitter)
           %@@@@* ,&@@@@*
          &@@@@@@@@@@@#
         %@@@@@@@@@@@*
       .&@@@@.  %@@@@#
       &@@@@/    %@@@@*
      %@@@@,      %@@@@#

  `;
  console.log(msg, 'color: #04e898');
}

if (document.getElementById('mainPage') != undefined && document.getElementById('mainPage') != null) {

  //Elements
  let portfolioTemplate = `<div class="projects--grid">{{header}}<div class="projects--grid--body">{{body}}</div></div>`,
    portfolioHeader = `<div class="projects--grid--header"><div class="projects--grid--header--title">{{title}}</div><span class="projects--grid--header--subtitle"></span><br></div>`,
    portfolioSlideItem = `<div class="projects--grid--body--item {{colour_profile}}--item" onMouseOver="this.querySelector('.projects--grid--body--item--content--avatar').style.backgroundImage = 'url(/public/images/work/icons/{{icon_hover_url}})'" onMouseOut="this.querySelector('.projects--grid--body--item--content--avatar').style.backgroundImage = 'url(/public/images/work/icons/{{icon_url}})'" onclick="window.location.href='/{{path_link}}'"><div class="projects--grid--body--item--content "><div class="projects--grid--body--item--content--title">{{title}}</div><span class="projects--grid--body--item--content--subtitle">{{shortDesc}}</span><div style="background-image: url('/public/images/work/icons/{{icon_url}}');"  class="projects--grid--body--item--content--avatar"></div></div></div>`;

  switch (window.location.pathname) {
    case "/":
      //Page Routing Logic
      let map = require('./map.json');
      let routes = Object.keys(map);
      for (var i = 0; i < routes.length; i++) {
        let route_items = Object.values(map[routes[i]]);
        let route = routes[i].replace(/_/g, " ").toUpperCase();

        let bodyTemplate = portfolioTemplate;
        let headerTemplate = portfolioHeader;
        headerTemplate = headerTemplate.replace("{{title}}", route);
        bodyTemplate = bodyTemplate.replace("{{header}}", headerTemplate);
        let innerContent = "";
        for (var k = 0; k < route_items.length; k++) {
          let item = route_items[k];
          let portfolioContent = portfolioSlideItem;
          portfolioContent = portfolioContent.replace("{{icon_url}}", "");
          portfolioContent = portfolioContent.replace("{{icon_hover_url}}", "");
          portfolioContent = portfolioContent.replace("{{title}}", item.title);
          portfolioContent = portfolioContent.replace("{{shortDesc}}", item.shortDesc);
          portfolioContent = portfolioContent.replace("{{link}}", item.link);
          portfolioContent = portfolioContent.replace("{{colour_profile}}", item.color);
          portfolioContent = portfolioContent.replace("{{path_link}}", "pages/" + item.path);
          innerContent += portfolioContent;
        }

        bodyTemplate = bodyTemplate.replace("{{body}}", innerContent);
        document.querySelector("#features").innerHTML += bodyTemplate;
      }
      break;
    default:
      let path_regex = /pages\/year_[0-9]\/[a-zA-Z0-9\-]+\/[a-zA-Z0-9\-]+[\/]?/g;
      let matched_routes = window.location.pathname.match(path_regex);
      if (matched_routes.length > 0) {
        let path = matched_routes[0].replace("pages/", "").replace(/\-/g, "_").split("/").filter((pathItem) => pathItem.length > 0).join("/");
        let year = matched_routes[0].match(/year_[0-9]/g)[0];
        let portfolio = require('./' + path + '/map.json');
        let weeknames = Object.keys(portfolio);
        for (var x = 0; x < weeknames.length; x++) {
          var bodyTemplate = portfolioTemplate;
          var headerTemplate = portfolioHeader;
          headerTemplate = headerTemplate.replace("{{title}}", weeknames[x]);
          bodyTemplate = bodyTemplate.replace("{{header}}", headerTemplate)
          var innerContent = "";
          for (var y = 0; y < portfolio[weeknames[x]].length; y++) {
            var portfolioContent = portfolioSlideItem;
            portfolioContent = portfolioContent.replace("{{icon_url}}", year + "/" + portfolio[weeknames[x]][y].path + "/" + portfolio[weeknames[x]][y].icon);
            portfolioContent = portfolioContent.replace("{{icon_url}}", year + "/" + portfolio[weeknames[x]][y].path + "/" + portfolio[weeknames[x]][y].icon);
            portfolioContent = portfolioContent.replace("{{icon_hover_url}}", year + "/" + portfolio[weeknames[x]][y].path + "/" + portfolio[weeknames[x]][y].hover_icon);
            portfolioContent = portfolioContent.replace("{{title}}", portfolio[weeknames[x]][y].title);
            portfolioContent = portfolioContent.replace("{{shortDesc}}", portfolio[weeknames[x]][y].shortDesc);
            portfolioContent = portfolioContent.replace("{{link}}", portfolio[weeknames[x]][y].link);
            portfolioContent = portfolioContent.replace("{{colour_profile}}", portfolio[weeknames[x]][y].color);
            portfolioContent = portfolioContent.replace("{{path_link}}", matched_routes.join("/") + portfolio[weeknames[x]][y].urlpath);
            innerContent += portfolioContent;
          }
          bodyTemplate = bodyTemplate.replace("{{body}}", innerContent)
          document.querySelector("#features").innerHTML += bodyTemplate;
        }

      } else {
        window.location.href = "/";
      }
  }
}
