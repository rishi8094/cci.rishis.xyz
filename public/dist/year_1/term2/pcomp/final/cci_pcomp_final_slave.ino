#include <LiquidCrystal.h>
#include <Wire.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  //Led
   pinMode(13, OUTPUT);

  //Slave
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.print("PLANT MONITOR");
  lcd.setCursor(0,1);
  lcd.print("STREAMING...");
  Wire.begin(8);
  Wire.onReceive(dataReceived);
  //pinMode(8, OUTPUT);
}

void loop() {
}


void dataReceived(int howMany)
{
  int temperature = Wire.read();
  int light = Wire.read();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(temperature);
  lcd.print("C");
  lcd.setCursor(4,0);
  lcd.print(light);
  lcd.print("SI");

  lcd.setCursor(0,1);
  if (temperature > 37){
  	lcd.print("TOO HOT!");
    digitalWrite(13, HIGH);
  }else{
  	lcd.print("HEALTHY");
  }
}
