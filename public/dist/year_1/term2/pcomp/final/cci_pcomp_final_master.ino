#include <Wire.h>

//Light
#include <Adafruit_NeoPixel.h>
#define NEOPIN 2	 // input pin Neopixel is attached to
#define NUMPIXELS      12 // number of neopixels in strip
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, NEOPIN, NEO_GRB + NEO_KHZ800);
int delayval = 100; // timing delay in milliseconds
int redColor = 0;
int greenColor = 0;
int blueColor = 0;

//Master
int tempC = 0;

void setup() {
  //Light
  pixels.begin();

  //Master
  delay(500);
  Wire.begin();
  Serial.begin(9600);
}

void loop(){

  //Master
  Wire.beginTransmission(8);
  tempC = currentTemp();
  Wire.write(tempC);
  int light = lightLevel();
  Wire.write(light);
  Wire.endTransmission();

  //Light
  setColor();
  for (int i=0; i < NUMPIXELS; i++) {
    if(light < 7){
      pixels.setPixelColor(i, pixels.Color(redColor, greenColor, blueColor));
    }else{
      pixels.setPixelColor(i, pixels.Color(0, 0, 0));
    }

    pixels.show();
  }
  delay(750);
}

//Master
int currentTemp(){
  int reading = analogRead(0);
  float voltage = reading * 5.0;
  voltage /= 1024.0;
  float temperatureC = (voltage - 0.5) * 100 ;
  return (int)temperatureC;
}

int lightLevel(){
  int sensorValue = analogRead(1);
  return sensorValue;
}

//Light
void setColor(){
  redColor = random(0, 255);
  greenColor = random(0,255);
  blueColor = random(0, 255);
}
