int switch_STATE = 0;
int green_LED = 3;
int yellow_LED = 4;
int red_LED = 5;
int pin_SWITCH = 2;

boolean oldswitch_STATE = LOW;
boolean newswitch_STATE1 = LOW;
boolean newswitch_STATE2 = LOW;
boolean newswitch_STATE3 = LOW;

byte state = 0;

void setup(){
    Serial.begin(9600);

    pinMode(green_LED, OUTPUT);
    digitalWrite(green_LED,LOW);

    pinMode(yellow_LED, OUTPUT);
    digitalWrite(yellow_LED,LOW);

    pinMode(red_LED, OUTPUT);
    digitalWrite(red_LED,LOW);

    pinMode(pin_SWITCH, INPUT);
}

void loop()
{
    newswitch_STATE1 = digitalRead(pin_SWITCH);
    delay(1);
    newswitch_STATE2 = digitalRead(pin_SWITCH);
    delay(1);
    newswitch_STATE3 = digitalRead(pin_SWITCH);

    if ((newswitch_STATE1==newswitch_STATE2) && (newswitch_STATE1==newswitch_STATE3)) {
        if ( newswitch_STATE1 != oldswitch_STATE ){
           if ( newswitch_STATE1 == HIGH ){
                state++;
                switch (state) {
                  case 1:
                    digitalWrite(green_LED, HIGH);
                    break;
                  case 2:
                    digitalWrite(yellow_LED, HIGH);
                    break;
                  case 3:
                    digitalWrite(red_LED, HIGH);
                    break;
                  default:
                    state = 0;
                    digitalWrite(green_LED, LOW);
                    digitalWrite(yellow_LED, LOW);
                    digitalWrite(red_LED, LOW);
                }
           }
           oldswitch_STATE = newswitch_STATE1;
        }
    }
}
