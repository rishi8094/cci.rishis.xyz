void setup() {
   Serial.begin(9600);
   Serial.println("Want to know something cool about metals?");
}

void loop() {
 if(Serial.available()) {
   String opt = Serial.readString();
   Serial.println(opt + "? Cool, I'm gonna take that as a SURE!");
   Serial.println("Tungsten has a very high melting point, after carbon it has the second highest melting point of all elements.");
 }
}
