//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.snake = function(p) {

  p.points = []
  p.c = 0;

  p.setup = function() {
    p.createCanvas(width, height);
    p.colorMode(p.HSB);
  }

  p.draw = function() {
    p.background(0);
    p.translate(width / 2, height / 2);
    let theta = p.millis() / 1000;
    let rad = 150;
    let x = p.cos(theta * 3) * rad;
    let y = p.sin(theta * 2) * rad;

    p.points.push({
      x: x,
      y: y
    })

    p.c += 1;
    if (p.c > 360) {
      p.c = 0;
    }

    var temp = p.points.slice(Math.max(p.points.length - 40, 0));
    p.points = temp;
    p.stroke(p.c, p.c, p.c)
    for (i = 1; i < temp.length; i++) {
      p.strokeWeight(i);
      p.line(temp[i].x, temp[i].y, temp[i - 1].x, temp[i - 1].y);
    }

  }

};

((window, document) => {
  setTimeout(()=>{
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.snakeCanvas = new p5(window.snake, document.getElementById('canvas'));
    window.snakeCanvas.resizeCanvas(newWidth, newHeight);
  }, 300)
})(window, document);
