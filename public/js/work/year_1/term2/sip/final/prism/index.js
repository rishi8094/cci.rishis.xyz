//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.prism = function(p) {

  p.static = {
    blobs: 1,
    km: p.random(0.6, 1.0),
    loops: 0.01,
    radius: 50,
    crossSize: 1,
    bubbleNoise: 200,
    time: 200
  }

  p.setup = () => {
    p.createCanvas(width, height);
    p.angleMode(p.DEGREES);
    p.noFill();
    p.noStroke();
  }

  p.draw = () => {
    p.blendMode(p.BLEND);
    p.background(0);
    p.blendMode(p.ADD);
    for (let i = p.static.blobs; i > 0; i--) {
      let time = p.frameCount / p.static.time;
      let size = p.static.radius + i * p.static.crossSize;
      let k = p.static.km * p.sqrt(i / p.static.blobs);
      for (var j = 0; j < 3; j++) {
        switch (j) {
          case 0:
            p.fill(255, 0, 0, 255);
            break;
          case 1:
            p.fill(0, 255, 0, 255);
            break
          default:
            p.fill(0, 0, 255, 255);
        }
        p.prismify(size, p.width / 2, p.height / 2, k, time - i * p.static.loops + (j * 2), (p.static.bubbleNoise * (i / p.static.blobs)));
      }
    }
  }

  p.prismify = (s, xc, yc, k, t, n) => {
    p.beginShape();
    let angleloops = 360 / 8;
    for (let theta = 0; theta <= 360 + 2 * angleloops; theta += angleloops) {
      let radius = s + p.noise(k * (p.cos(theta) + 1), k * (p.sin(theta) + 1), t) * n;
      p.curveVertex((xc + radius * p.cos(theta)), (yc + radius * p.sin(theta)));
    }
    p.endShape();
  }
};


((window, document) => {
  setTimeout(() => {
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.prismCanvas = new p5(window.prism, document.getElementById('canvas'));
    window.prismCanvas.resizeCanvas(newWidth, newHeight);
  }, 100)
})(window, document);
