//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.sinwave = function(p) {

  p.colors = [];

  p.setup = () => {
    p.createCanvas(width, height);
    p.colors = [
      p.color(255, 0, 0),
      p.color(0, 255, 0),
      p.color(0, 0, 255)
    ];
  }

  p.draw = () => {
    p.blendMode(p.BLEND);
    p.background(0);
    p.blendMode(p.SCREEN);
    p.noFill();
    p.strokeWeight(20);
    p.stroke(255);
    for (let x = 0; x < p.colors.length; x++) {
      p.stroke(p.colors[x]);
      p.beginShape();
      for (let y = -20; y < p.width + 20; y += 5) {
        var h = p.height / 2;
        h += p.width/2 * p.sin(y * 0.03 + p.frameCount * 0.07 + x * p.TWO_PI / 3) * p.pow(p.abs(p.sin(y * 0.001 + p.frameCount * 0.02)), 9);
        p.curveVertex(y, h);
      }
      p.endShape();
    }
  }
};


((window, document) => {
  setTimeout(() => {
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.sinwaveCanvas = new p5(window.sinwave, document.getElementById('canvas'));
    window.sinwaveCanvas.resizeCanvas(newWidth, newHeight);
  }, 100)
})(window, document);
