//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.morph = function(p) {

  p.global = {
    inner: {
      radius: 180,
      amplitude: 0,
      x: 0,
      y: 0
    },
    outer: {
      radius: 100,
      amplitude: 0,
      x: 0,
      y: 0
    }
  }

  p.setup = () => {
    p.createCanvas(width, height);
    p.fill(10);
    p.stroke(255);
    p.strokeWeight(3);
  }

  p.draw = () => {
    p.frameRate(30);
    p.background(0);
    p.translate(width / 2, height / 2);
    p.rotate(p.frameCount * 0.005);
    p.beginShape(p.QUAD_STRIP);
    for (let angle = 0; angle <= p.TWO_PI; angle += p.PI/30) {
      p.global.outer.x = (p.global.outer.radius + p.global.outer.amplitude * p.sin(5 * angle)) * p.cos(angle);
      p.global.outer.y = (p.global.outer.radius + p.global.outer.amplitude * p.cos(4 * angle)) * p.sin(angle);
      p.global.inner.x = (p.global.inner.radius + p.global.inner.amplitude * p.sin(4 * angle)) * p.cos(angle);
      p.global.inner.y = (p.global.inner.radius + p.global.inner.amplitude * p.cos(3 * angle)) * p.sin(angle);
      p.vertex(p.global.inner.x, p.global.inner.y);
      p.vertex(p.global.outer.x, p.global.outer.y);
    }
    p.endShape(p.CLOSE);
    p.global.inner.amplitudep = p.sin(p.frameCount * .01) * 50;
    p.global.outer.amplitude = p.sin(p.frameCount * .019) * 50;
    p.global.outer.radius = 100 + p.sin(p.frameCount * 0.013) * 50;
    p.global.inner.radius = 180 + p.sin(p.frameCount * 0.016) * 50;
  }

};

((window, document) => {
  setTimeout(() => {
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.morphCanvas = new p5(window.morph, document.getElementById('canvas'));
    window.morphCanvas.resizeCanvas(newWidth, newHeight);
  }, 500)
})(window, document);
