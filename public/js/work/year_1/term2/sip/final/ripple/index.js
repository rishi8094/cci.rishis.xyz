//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.ripple = function(p) {

  p.ringCount = 0;
  p.c = 0;
  p.setup = function() {
    p.createCanvas(width, height);
    p.colorMode(p.HSB)
  }

  p.draw = function() {
    p.c += 1;
    if (p.c > 360) {
      p.c = 0;
    }
    p.background(0);
    for (var i = 0; i < 250; i++) {
      let rings = p.ringCount - 20 * i;
      if (rings > 0) {
        var fade = p.map(rings, 0, width, 255, 0);
        p.stroke(fade);
        console.log(i);
        if (i < 5) {
          p.fill(p.c, p.c, p.c);
        } else {
          p.noFill();
        }
        p.ellipse(p.width / 2, p.height / 2, rings);
      }

    }

    p.ringCount = p.ringCount + 2;
  }

};

((window, document) => {
  setTimeout(() => {
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.rippleCanvas = new p5(window.ripple, document.getElementById('canvas'));
    window.rippleCanvas.resizeCanvas(newWidth, newHeight);
  }, 100)
})(window, document);
