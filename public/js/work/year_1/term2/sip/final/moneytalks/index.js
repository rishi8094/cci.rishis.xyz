//OVERRIDE
var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.moneytalks = function(p) {
  p.image;
  p.sound;
  p.mouse_pressed_colour;

  p.preload = () => {
    p.image = p.loadImage("/public/images/portfolio/year_1/moneytalks/dollar.png");
    p.sound = p.loadSound("/public/dist/year_1/term2/sip/moneytalks/success.wav");
  }

  p.setup = () => {
    p.createCanvas(700, 500);
    p.background(0);
  }

  p.draw = () => {
    p.image.loadPixels();
    for (var y = 0; y < p.image.height; y += 10) {
      for (var x = 0; x < p.image.width; x += 10) {
        var r = p.image.pixels[(y * p.image.width + x) * 4];
        var g = p.image.pixels[(y * p.image.width + x) * 4 + 1];
        var b = p.image.pixels[(y * p.image.width + x) * 4 + 2];
        p.fill(r, g, b);
        p.rect(x, y, 8, 8);
        p.mouse_pressed_colour = p.image.get(p.mouseX, p.mouseY);
      }
    }
  }

  p.mousePressed = () => {
    let r = p.mouse_pressed_colour[0],
      g = p.mouse_pressed_colour[1],
      b = p.mouse_pressed_colour[2];
    if (r == 255 && g == 255 && b == 255) {
      p.sound.play();
    }

  }

};


((window, document) => {
  setTimeout(() => {
    let newWidth = document.querySelector("#canvas").clientWidth;
    let newHeight = document.querySelector("#canvas").clientHeight;
    window.moneytalksCanvas = new p5(window.moneytalks, document.getElementById('canvas'));
    window.moneytalksCanvas.resizeCanvas(newWidth, newHeight);
  }, 100)
})(window, document);
