var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;


window.design = function(p) {
  p.bearHead;
  p.bearBody;
  p.bbW = 350;
  p.bbH = (p.bbW/750) * 742;
  p.size = 150;
  p.x = (p.width + 30);
  p.y = (p.height + 35);
  p.easing = 0.05;
  p.motion = 0;

  p.setup = function() {
    p.createCanvas(width, height);
  }

  p.preload = function() {
    p.bearBody = p.loadImage("/public/images/portfolio/year_1/week4/polarbear.png")
    p.bearHead = p.loadImage("/public/images/portfolio/year_1/week4/polarhead.png")
  }

  p.draw = function() {
    p.motion += 0.1;

    let targetY = p.sin(p.motion) * 30;
    let dy = targetY - p.y;
    p.y += dy * p.easing;
    p.image(p.bearBody, ((p.width - p.bbW)/2), ((p.height - p.bbH)/2) - p.x - 10, p.bbW, p.bbH);
    p.image(p.bearHead, p.x, p.y, p.size, p.size);

    // Glowing Eyes
    // p.fill("#ff0000");
    // p.push();
    // p.beginShape();
    // p.vertex(213, 153);
    // p.vertex(227, 141);
    // p.vertex(228, 150);
    // p.vertex(218, 156);
    // p.endShape(p.CLOSE);
    // p.pop();
    //
    // p.push();
    // p.beginShape();
    // p.vertex(159, 142);
    // p.vertex(172, 153);
    // p.vertex(168, 156);
    // p.vertex(159, 150);
    // p.endShape(p.CLOSE);
    // p.pop();
  }

  p.mousePressed = function() {
    console.log(p.y);
    console.log(p.mouseX, p.mouseY);
  }

};

window.designCanvas = new p5(window.design, document.getElementById('canvas'));
