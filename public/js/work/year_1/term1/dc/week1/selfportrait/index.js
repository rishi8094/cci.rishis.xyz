var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.portrait = function(p) {
  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height);
  }
  p.draw = function() {
    p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
    p.background(p.color("#3b3b46"));
    p.push()
    p.strokeWeight(10);
    p.fill("#FFCA3B");
    p.circle(width / 2, height / 2, 250)
    p.pop()

    p.push()
    p.fill("#000000")
    p.rect(width / 2 + 40, height / 2 - 50, 20, 40, 20);
    p.rect(width / 2 - 50, height / 2 - 50, 20, 40, 20);
    p.pop()

    p.push()
    p.fill("#000000")
    p.rect(width / 2 - 30, height / 2 + 40, 70, 10, 20);
    p.pop()
  }

};

window.portraitCanvas = new p5(window.portrait, document.getElementById('canvas'));
