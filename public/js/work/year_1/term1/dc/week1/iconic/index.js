var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.design = function(p) {
  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height);
  }

  p.draw = function() {
    p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
      p.background("#fff");
      p.strokeWeight(5);
      p.push();
      p.fill("#E5E5E5")
      p.rect(0, 0, Math.ceil(p.width * (1/5)), 50);
      p.rect(0 + Math.ceil(p.width * (1/5)), 0, Math.ceil(p.width * (3/5)), 50);
      p.pop();

      p.push();
      p.fill("#E2B001");
      p.rect(0 + Math.ceil(p.width * (4/5)), 0, Math.ceil(p.width * (1/5)), 50);
      p.pop();

      p.push();
      p.fill("#231EC0");
      p.rect(0, 50, Math.ceil(p.width * (1/7)),  p.height - 50);
      p.pop();

      p.push();
      p.fill("#C60001");
      p.rect(0 + Math.ceil(p.width * (1/7)), 50, Math.ceil(p.width * (4.6/7)), p.height - 100);
      p.pop();

      p.push();
      p.fill("#000000");
      p.rect(0 + Math.ceil(p.width * (5.6/7)), 50, Math.ceil(p.width * (1.4/7)), 130);
      p.pop();

      p.push();
      p.fill("#E5E5E5");
      p.rect(0 + Math.ceil(p.width * (5.6/7)), 180, Math.ceil(p.width * (0.8/7)), 70);
      p.rect(0 + Math.ceil(p.width * (6.4/7)), 180, Math.ceil(p.width * (0.6/7)), 70);
      p.pop();

      p.push();
      p.fill("#000");
      p.rect(0 + Math.ceil(p.width * (1/7)),  p.height - 50, Math.ceil(p.width * (3/7)), 50);
      p.pop();

  }
};
window.designCanvas = new p5(window.design, document.getElementById('canvas'));
