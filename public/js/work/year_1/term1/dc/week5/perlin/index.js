var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;


window.design = function(p) {

  p.t = 0;

  p.setup = function() {
    p.createCanvas(width, height);
  }

  p.draw = function() {
      p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
    p.background(0);
    p.fill(255);
    p.t += 0.01;

    var c1 = p.noise(p.t);
    c1 = p.map(c1, 0, 1, 0, p.width)
    p.ellipse(c1, p.height/2, 40, 40)

    p.ellipse(c1 - p.width/5, p.height/3, 40, 40)

    p.ellipse(c1 + p.width/5 , p.height - 100, 40, 40)

    p.ellipse(c1, p.height - 50, 40, 40)

    p.ellipse(c1,  50, 40, 40)
  }

};

window.designCanvas = new p5(window.design, document.getElementById('canvas'));
