var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.proxyGlobe = function(p) {

  p.datasource = [];

  p.angle = 0;

  p.earth = p.loadImage("/public/images/portfolio/year_1/final/proxygrid/earth.jpg");

  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height, p.WEBGL);
    document.getElementById('canvas').style.height = `${height}px`;
    document.getElementById('canvas').style.width = `${width}px`;
  }
  p.draw = function() {
    p.background(p.color("#3b3b46"))
    p.translate(0, -10);
    p.rotateY(p.angle);
    p.angle += 0.05;

    p.lights();
    p.fill(200);
    p.noStroke();
    var r = 120;
    p.texture(p.earth)
    p.sphere(r)

    if (p.datasource.length > 0) {
      for (var i = 0; i < p.datasource.length; i++) {
        var theta = p.radians(p.datasource[i].lat) + p.PI / 2;
        var phi = p.radians(p.datasource[i].lon) + p.PI;
        var x = r * p.sin(theta) * p.cos(phi);
        var y =  - r * p.sin(theta) * p.sin(phi);
        var z = r * p.cos(theta);
        var pos = p.createVector(x, y, z)
        var h = p.pow(10, Math.ceil(p.datasource[i].proxies > 30 ? (p.datasource[i].proxies/10):  p.datasource[i].proxies*0.005));
        var maxH = p.pow(10, 10.5);
        h = p.map(h, 0, maxH, 10, 100);

        var xaxis = p.createVector(1,0,0);
        var angleb = xaxis.angleBetween(pos)
        var raxis = xaxis.cross(pos);
        p.push();
        p.translate(x, y, z);
        p.rotate(angleb, raxis);
        var color = "#034FFE";
        if (p.datasource[i].proxies > 10) {
          color = "#1EDE01";
        }
        if (p.datasource[i].proxies > 30) {
          color = "#FEC201";
        }
        if (p.datasource[i].proxies > 50) {
          color = "#FC2756";
        }
        p.fill(color);
        p.box(h, 5, 5);
        p.pop();

      }
    }
  }

};

window.proxyCanvas = new p5(window.proxyGlobe, document.getElementById('canvas'));

class ProxyGrid {
  constructor() {
    this.datasource = [];
    this.rawdata = [];
    this.profiles = 0;
    this.proxies = 0;
    this.active = false;
  }
  getSources() {
    return this.datasource;
  }

  async init() {
    try {
      var resp = await fetch("https://api.theproxylab.io/analytics?key=bT4ZwwrgLBEQ68ryS5qvnrj5pDszhD", {
        method: "GET"
      });
      var json = await resp.json();
      if (!json.success) {
        throw "invalid request"
      }
      this.active = true;
      this.profiles = json.profiles;
      this.proxies = json.proxies;
      this.rawdata = json.data;
      this.dataHandler();
      this.runGlobe();
    } catch (e) {
      this.active = false;
    }
  }
  runGlobe() {
    window.proxyCanvas.datasource = this.datasource;
  }
  async dataHandler() {
    this.rawdata.forEach((data) => {
      var region = data.region;
      region = region.replace("-a", "").replace("-b", "");
      var proxy = {
        lon: 0,
        lat: 0,
        proxies: data.proxies,
        region: region
      }
      switch (region) {
        case "asia-east1":
          proxy.lon = 24.0518;
          proxy.lat = 120.5161;
          break;
        case "asia-east2":
          proxy.lon = 22.3193;
          proxy.lat = 114.1694;
          break;
        case "asia-northeast1":
          proxy.lon = 35.6762;
          proxy.lat = 139.6503;
          break;
        case "asia-northeast1-c":
          proxy.lon = 35.6762;
          proxy.lat = 139.6503;
          break;
        case "asia-northeast2":
          proxy.lon = 34.6937;
          proxy.lat = 135.5023;
          break;
        case "asia-south1":
          proxy.lon = 19.0760;
          proxy.lat = 72.8777;
          break;
        case "asia-south1-c":
          proxy.lon = 19.0760;
          proxy.lat = 72.8777;
          break;
        case "asia-southeast1":
          proxy.lon = 1.3404;
          proxy.lat = 103.7090;
          break;
        case "australia-southeast1":
          proxy.lon = 33.8688;
          proxy.lat = 151.2093;
          break;
        case "europe-north1":
          proxy.lon = 60.5693;
          proxy.lat = 27.1878;
          break;
        case "europe-west1":
          proxy.lon = 50.4491;
          proxy.lat = 3.8184;
          break;
        case "europe-west1-c":
          proxy.lon = 50.4491;
          proxy.lat = 3.8184;
          break;
        case "europe-west1-d":
          proxy.lon = 50.4491;
          proxy.lat = 3.8184;
          break;
        case "europe-west2":
          proxy.lon = 51.5074;
          proxy.lat = 0.1278;
          break;
        case "europe-west2-c":
          proxy.lon = 51.5074;
          proxy.lat = 0.1278;
          break;
        case "europe-west3":
          proxy.lon = 50.1109;
          proxy.lat = 8.6821;
          break;
        case "europe-west3-c":
          proxy.lon = 50.1109;
          proxy.lat = 8.6821;
          break;
        case "europe-west4":
          proxy.lon = 53.4386;
          proxy.lat = 6.8355;
          break;
        case "europe-west4-c":
          proxy.lon = 53.4386;
          proxy.lat = 6.8355;
          break;
        case "europe-west6":
          proxy.lon = 47.3769;
          proxy.lat = 8.5417;
          break;
        case "northamerica-northeast":
          proxy.lon = 45.5017;
          proxy.lat = 73.5673;
          break;
        case "northamerica-northeast1":
          proxy.lon = 45.5017;
          proxy.lat = 73.5673;
          break;
        case "southamerica-east1":
          proxy.lon = 23.5505;
          proxy.lat = 46.6333;
          break;
        case "southamerica-east1-c":
          proxy.lon = 23.5505;
          proxy.lat = 46.6333;
          break;
        case "us-central1":
          proxy.lon = 41.2619;
          proxy.lat = 95.8608;
          break;
        case "us-central1-c":
          proxy.lon = 41.2619;
          proxy.lat = 95.8608;
          break;
        case "us-east1":
          proxy.lon = 33.1960;
          proxy.lat = 80.0131;
          break;
        case "us-east1-c":
          proxy.lon = 33.1960;
          proxy.lat = 80.0131;
          break;
        case "us-east1-d":
          proxy.lon = 33.1960;
          proxy.lat = 80.0131;
          break;
        case "us-east4":
          proxy.lon = 39.0403;
          proxy.lat = -77.4852;
          break;
        case "us-east4-c":
          proxy.lon = 39.0403;
          proxy.lat = -77.4852;
          break;
        case "us-east4-d":
          proxy.lon = 39.0403;
          proxy.lat = -77.4852;
          break;
        case "us-west1":
          proxy.lon = 45.5946;
          proxy.lat = 121.1787;
          break;
        case "us-west1-c":
          proxy.lon = 45.5946;
          proxy.lat = 121.1787;
          break;
        case "us-west2":
          proxy.lon = 34.0522;
          proxy.lat = 118.2437;
          break;
        case "us-east-2":
          proxy.lon = 40.367474;
          proxy.lat = -82.996216;
          break;
        case "us-east-1":
          proxy.lon = 39.0438;
          proxy.lat = 77.4874;
          break;
        case "us-west-1":
          proxy.lon = 38.8375;
          proxy.lat = 120.8958;
          break;
        case "us-west-2":
          proxy.lon = 43.8041;
          proxy.lat = 120.5542;
          break;
        case "ap-east-1":
          proxy.lon = 22.3193;
          proxy.lat = 114.1694;
          break;
        case "ap-south-1":
          proxy.lon = 19.0760;
          proxy.lat = 72.8777;
          break;
        case "ap-northeast-3":
          proxy.lon = 34.6937;
          proxy.lat = 135.5023;
          break;
        case "ap-northeast-2":
          proxy.lon = 37.5665;
          proxy.lat = 126.9780;
          break;
        case "ap-southeast-1":
          proxy.lon = 1.3521;
          proxy.lat = 103.8198;
          break;
        case "ap-southeast-2":
          proxy.lon = 33.8688;
          proxy.lat = 151.2093;
          break;
        case "ap-northeast-1":
          proxy.lon = 35.6762;
          proxy.lat = 139.6503;
          break;
        case "ca-central-1":
          proxy.lon = 56.1304;
          proxy.lat = 106.3468;
          break;
        case "cn-north-1":
          proxy.lon = 39.9042;
          proxy.lat = 116.4074;
          break;
        case "cn-northwest-1":
          proxy.lon = 37.1987;
          proxy.lat = 106.1581;
          break;
        case "eu-central-1":
          proxy.lon = 50.1109;
          proxy.lat = 8.6821;
          break;
        case "eu-west-1":
          proxy.lon = 53.1424;
          proxy.lat = 7.6921;
          break;
        case "eu-west-2":
          proxy.lon = 51.5074;
          proxy.lat = 0.1278;
          break;
        case "eu-west-3":
          proxy.lon = 48.8566;
          proxy.lat = 2.3522;
          break;
        case "eu-north-1":
          proxy.lon = 59.3293;
          proxy.lat = 18.0686;
          break;
        case "sa-east-1":
          proxy.lon = 23.5505;
          proxy.lat = 46.6333;
          break;
        case "New York 1":
          proxy.lon = 40.7128;
          proxy.lat = 74.0060;
          break;
        case "New York 2":
          proxy.lon = 40.7128;
          proxy.lat = 74.0060;
          break;
        case "New York 3":
          proxy.lon = 40.7128;
          proxy.lat = 74.0060;
          break;
        case "Amsterdam 2":
          proxy.lon = 52.3667;
          proxy.lat = 4.8945;
          break;
        case "Amsterdam 3":
          proxy.lon = 52.3667;
          proxy.lat = 4.8945;
          break;
        case "San Francisco 1":
          proxy.lon = 37.7749;
          proxy.lat = 122.4194;
          break;
        case "San Francisco 2":
          proxy.lon = 37.7749;
          proxy.lat = 122.4194;
          break;
        case "Singapore 1":
          proxy.lon = 1.3521;
          proxy.lat = 103.8198;
          break;
        case "London 1":
          proxy.lon = 51.5074;
          proxy.lat = 0.1278;
          break;
        case "Frankfurt 1":
          proxy.lon = 50.1109;
          proxy.lat = 8.6821;
          break;
        case "Toronto 1":
          proxy.lon = 43.6532;
          proxy.lat = 79.3832;
          break;
        case "Bangalore 1":
          proxy.lon = 12.9716;
          proxy.lat = 77.5946;
          break;
        default:
          break;
      }
      this.datasource.push(proxy);
    })
  }
}


window.ProxyGrid = new ProxyGrid();
window.ProxyGrid.init();
