var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.recaptchav4 = function(p) {
  p.storyBoard = {
    box: false
  }

  p.gridLayout = [];

  p.setup = function() {
    p.createCanvas(width, height);
    document.getElementById('captchaFrame').style.height = `${height}px`;
    document.getElementById('captchaFrame').style.width = `${width}px`;
  }

  p.mousePressed = function() {
    p.selectSquare()
  }

  p.draw = function() {
    p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
    if (!p.storyBoard.box) {
      p.reGrid();
    }
  }

  p.drawBG = function(images) {
    var grid = {
      item: {
        height: (height - (10 * 2)) / 3,
        width: (width - (10 * 2)) / 3
      }
    }

    var imgcounter = 0;
    for (var y = 0; y < 3; y++) {
      var yoff = 10 + (grid.item.height * y);
      for (var x = 0; x < 3; x++) {
        document.getElementById('captchaFrame').innerHTML += `<div style="position:absolute;height:${grid.item.height}px;width:${grid.item.width}px;top:${yoff}px;left:${10 + (grid.item.width * x)}px;background:url('${images[imgcounter].url}')" captcha-hash="${images[imgcounter].hash}" class="active__image__captcha"></div>`
        p.stroke("#ddd");
        p.rect(10 + (grid.item.width * x), yoff, grid.item.width, grid.item.height)
        p.gridLayout.push({
          x: (10 + (grid.item.width * x)),
          y: yoff,
          w: grid.item.width,
          h: grid.item.height
        })
        imgcounter += 1;
      }
    }
  }

  p.reGrid = function() {
    p.storyBoard.box = true;
    var grid = {
      item: {
        height: (height - (10 * 2)) / 3,
        width: (width - (10 * 2)) / 3
      }
    }
    for (var y = 0; y < 3; y++) {
      var yoff = 10 + (grid.item.height * y);
      for (var x = 0; x < 3; x++) {
        p.stroke("#ddd");
        p.rect(10 + (grid.item.width * x), yoff, grid.item.width, grid.item.height)
      }
    }
  }

  p.selectSquare = function() {
    for (var i = 0; i < p.gridLayout.length; i++) {
      var xMin = (p.gridLayout[i].x),
        xMax = p.gridLayout[i].x + (p.gridLayout[i].w),
        yMin = p.gridLayout[i].y,
        yMax = p.gridLayout[i].y + (p.gridLayout[i].h);
      if ((xMin < p.mouseX && p.mouseX < xMax) && (yMin < p.mouseY && p.mouseY < yMax)) {
        // console.log(`Pressed Square ${i}`);
        if (document.getElementById('captchaFrame').children[i].style.opacity != 0.6) {
          document.getElementById('captchaFrame').children[i].style.opacity = 0.6;
          window.ReCaptcha.captcha.registerClick((document.getElementById('captchaFrame').querySelectorAll(".active__image__captcha"))[i].getAttribute("captcha-hash"));
          p.push();
          p.fill("blue");
          p.rect(xMin, yMin, p.gridLayout[i].w, p.gridLayout[i].h);
          p.pop();
        } else {
          document.getElementById('captchaFrame').children[i].style.opacity = 1;
          window.ReCaptcha.captcha.registerClick((document.getElementById('captchaFrame').querySelectorAll(".active__image__captcha"))[i].getAttribute("captcha-hash"));
        }
      }

    }
  }

};

window.captchaCanvas = new p5(window.recaptchav4, document.getElementById('canvas'));

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


class Captcha {
  constructor() {
    this.datasource = [];
    this.selected = []
    this.token = "";
    this.topic = "";
    this.active = false;
  }
  getSources() {
    return this.datasource;
  }
  async load() {
    try {
      var resp = await fetch("https://rishiscaptcha.now.sh/api/pull", {
        method: "POST"
      });
      var json = await resp.json();
      if (!json.success) {
        throw "invalid request"
      }
      this.active = true;
      this.topic = json.data.topic;
      this.datasource = json.data.images;
      this.token = json.data.token;
    } catch (e) {
      this.active = false;
    }
  }

  async validate() {
    try {
      var resp = await fetch("https://rishiscaptcha.now.sh/api/validate", {
        headers: {
          'Accept': 'application/json',
          'content-type': 'text/plain;api=json'
        },
        method: "POST",
        cors: true,
        body: JSON.stringify({
          token: this.token,
          hash: this.selected
        })
      });
      var json = await resp.json();
      if (!json.success) {
        throw "invalid request"
      }
      return json;
    } catch (e) {
      throw e
    }
  }

  registerClick(hash) {
    if (!this.selected.includes(hash)) {
      this.selected.push(hash);
    } else {
      this.selected.remove(hash)
    }

    if (this.selected.length > 0) {
      document.getElementById('validate').disabled = false;
      document.getElementById('validate').onclick = captchaValidator;
    } else {
      document.getElementById('validate').disabled = true;
    }
  }
}

function captchaValidator() {
  window.ReCaptcha.verify();
}

function captchaRestart() {
  document.getElementById('validate').classList.value = document.getElementById('validate').classList.value.replace("gradient__green", "");
  document.getElementById('validate').classList.value = document.getElementById('validate').classList.value.replace("gradient__red", "");
  document.getElementById('canvas').innerHTML = `<div style="position: absolute;z-index: 1100;" id="captchaFrame"></div>`;
  window.captchaCanvas = new p5(window.recaptchav4, document.getElementById('canvas'));
  window.ReCaptcha.captcha = new Captcha();
  window.ReCaptcha.start();

}

Array.prototype.remove = function() {
  var what, a = arguments,
    L = a.length,
    ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

class ReCaptcha {
  constructor() {
    this.timer = new Date();
    this.score = 0;
    this.round = 0;
    this.captcha = new Captcha();
  }
  async start() {
    try {
      await this.captcha.load();
      window.captchaCanvas.drawBG(this.captcha.getSources());
      document.getElementById('topic').innerHTML = this.captcha.topic;
    } catch (e) {
      console.log(e);
    }
  }

  async verify() {
    console.log(`⏳: ${(new Date().getTime() - this.timer)/1000}s || 💰: ${this.score} || ROUND: ${this.round}`);
    if (this.round > 2 && ((new Date().getTime() - this.timer)/1000) > 5) {
      var humanPresence = "ROBOT"
      if (this.score > 400) {
        humanPresence = "HUMAN";
      }
      alert(`SCORE: ${this.score} in: ${(new Date().getTime() - this.timer)/1000}s  ===> ${humanPresence}`);
      window.location.reload();
      return;
    }
    this.round += 1;
    document.getElementById('validate').classList.value = document.getElementById('validate').classList.value.replace("gradient__green", "");
    document.getElementById('validate').classList.value = document.getElementById('validate').classList.value.replace("gradient__red", "");
    try {
      var c = await this.captcha.validate();
      if (c.data.pass) {
        this.score += 200;
        document.getElementById('validate').classList.value += " gradient__green"
        document.getElementById('validate').innerHTML = "Human Detected";
        document.getElementById('validate').disabled = true;
        setTimeout(() => {
          document.getElementById('validate').innerHTML = "VERIFY";
          captchaRestart();
        }, 1000);
      } else {
        this.score -= 100;
        document.getElementById('validate').innerHTML = `<i style="font-size:20px" class="fas fa-spin fa-circle-notch"></i>`;
        setTimeout(() => {
          document.getElementById('validate').innerHTML = "VERIFY";
          captchaRestart();
        }, 750);
      }
    } catch (e) {
      console.log(e);
      document.getElementById('validate').classList.value += " gradient__red"
      document.getElementById('validate').innerHTML = "API ERROR";
      document.getElementById('validate').disabled = true;
    }
  }
}

window.ReCaptcha = new ReCaptcha();
window.ReCaptcha.start();
