//OVERRIDE
Dw.EasyCam.prototype.apply = function(n) {
  var o = this.cam;
  n = n || o.renderer,
    n && (this.camEYE = this.getPosition(this.camEYE), this.camLAT = this.getCenter(this.camLAT), this.camRUP = this.getUpVector(this.camRUP), n._curCamera.camera(this.camEYE[0], this.camEYE[1], this.camEYE[2], this.camLAT[0], this.camLAT[1], this.camLAT[2], this.camRUP[0], this.camRUP[1], this.camRUP[2]))
};
//OVERRIDE

var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.sphere = function(p) {
  p.globe = [];
  p.total = 1;
  p.r = 110;
  p.dotColor = randomColour();
  p.dotState = "increasing"

  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height, p.WEBGL);
    p.colorMode(p.RGB)
    p.easycam = new Dw.EasyCam(p._renderer, {
      distance: 500
    });

    p.easycam.pushState()
    p.easycam.rotateY(1000)
    p.easycam.zoom(-2)
    p.easycam.popState(100)

  }
  p.draw = function() {
    p.stroke(p.dotColor);
    if (p.total < 25) {
      p.strokeWeight(10);
    }
    if (p.total < 50) {
      p.strokeWeight(4);
    }

    if (p.total == 50) {
      p.total = 49;
      p.dotColor = randomColour();
      p.dotState = "decreasing";
    }

    if (p.total < 50) {
      if (p.dotState == "increasing") {
        p.total += 1;
      } else {
        p.total -= 1;
        if (p.total > 1) {

        } else {
          p.dotState = "increasing"
        }
      }
    }

    numberGen()



    p.easycam.pushState()
    p.easycam.rotateY(Math.floor(Math.random(500, 1000) * 1000))
    p.easycam.rotateX(Math.floor(Math.random(10, 100) * 100))
    p.easycam.popState(80)


    p.background(p.color("#3b3b46"));
    p.lights();
    p.translate(0, 0);

    //<--------------
    var iarray = [];
    for (var i = 0; i < p.total; i++) {
      var lon = p.map(i, 0, p.total, -p.PI, p.PI);
      var jarray = [];
      for (var j = 0; j < p.total; j++) {
        var lat = p.map(j, 0, p.total, -(p.HALF_PI), (p.HALF_PI));
        var x = p.r * Math.sin(lon) * Math.cos(lat);
        var y = p.r * Math.sin(lon) * Math.sin(lat);
        var z = p.r * Math.cos(lon);
        jarray.push(p.createVector(x, y, z));
      }
      iarray.push(jarray);
    }
    p.globe = iarray;
    //-------------->


    for (var i = 0; i < p.total; i++) {

      for (var j = 0; j < p.total; j++) {

        var vect1 = p.globe[i][j];
        p.point(vect1.x, vect1.y, vect1.z);

      }


    }

  }

};

function randomColour() {
  var a = numberGen();
  var b = numberGen();
  var c = numberGen();
  var colour = rgbToHex(a, b, c);
  console.log(`Changing colour to ${colour} [${a}, ${b}, ${c}]`);
  return colour;
}

function numberGen() {
  var timeStamp = (btoa(Date.now()).replace("=", "").replace("=", ""));
  var counter = 0;
  counter += timeStamp.charCodeAt(Math.floor(Math.random() * timeStamp.length));
  counter += timeStamp.charCodeAt(Math.floor(Math.random() * timeStamp.length));
  counter += timeStamp.charCodeAt(Math.floor(Math.random() * timeStamp.length));
  if (counter > 180) {
    var ded = Math.floor(Math.random() * 255);
    if (ded < 30) {
      ded = 30;
    }
    var deadWeight = counter - ded;
    if (deadWeight < 0) {
      var add = Math.floor(Math.random() * (Math.abs(deadWeight)));
      if (add < deadWeight) {
        counter = 0 + add;
      }else{
        counter += add;
      }
    }else{
      counter -= ded;
    }
  }
  return counter
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}


window.sphereCanvas = new p5(window.sphere, document.getElementById('canvas'));
