var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

let a = 0;

var config = {
  screen: {
    width: width - (width/2),
    height: height
  },
  fonts: ["Helvetica", "Helvetica Nueue"],
  data: [{
      image: "/public/images/portfolio/year_1/week3/mouse.jpg",
      caption: `"VIRGIL"`,
      sound: "/public/images/portfolio/year_1/week3/dissapear.mp3"
    },
    {
      image: "/public/images/portfolio/year_1/week3/mouse.png",
      caption: `"SWEAT"`,
      sound: "/public/images/portfolio/year_1/week3/future.mp3"
    },
    {
      image: "/public/images/portfolio/year_1/week3/virgil_4.jpg",
      caption: `"SECURE THE BAG"`,
      sound: "/public/images/portfolio/year_1/week3/travel.mp3",
      customSize: true,
      size: {
        width: width,
        height: height
      }
    }
  ]
}

var slide = [];
var sounds = [];

window.design = function(p) {
  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height);
    typography = p.random(config.fonts);
  }
  p.preload = function() {
    for (var i = 0; i < config.data.length; i++) {
      slide.push(p.loadImage(config.data[i].image));
      sounds.push(p.loadSound(config.data[i].sound));
    }
  }

  p.draw = function() {
    p.fill('white');

    if (config.data[a].customSize) {
      p.image(slide[a], 0, 0, config.data[a].size.width, config.data[a].size.height);
    } else {
      p.image(slide[a], width / 4, 0, config.screen.width, config.screen.height);
    }


    p.textFont(typography, 40); // Setup our text
    p.textAlign(p.CENTER, p.CENTER);
    p.text(config.data[a].caption, width / 2, height / 2);
  }

  p.mousePressed = function() {
    song = sounds[a];
    song.play();
    if (a >= (config.data.length - 1)) {
      a = -1;
    }
    a++;
    // slide = loadImage(pics[a]);
    // typography = random(myFonts);
  }

};

function randomColor() {
  var arr = ["#6284FF", "#52ACFF", "#FAACA8", "#DDD6F3", "#21D4FD", "#B721FF", "#08AEEA", "#2AF598", "#8EC5FC", "#E0C3FC", "#D9AFD9", "#97D9E1", "#0093E9", "#80D0C7", "#74EBD5", "#9FACE6", "#2BD2FF", "#2BFF88"]
  return arr[getRandomInt(0, arr.length - 1)]
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
window.designCanvas = new p5(window.design, document.getElementById('canvas'));
