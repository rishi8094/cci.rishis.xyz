var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;


window.design = function(p) {
  p.cols;
  p.rows;
  p.scl = 20;

  p.setup = function() {
    p.createCanvas(width, height, p.WEBGL);
    var w = p.width;
    var h = p.height;
    p.cols = Math.ceil(w / p.scl);
    p.rows = Math.ceil(h / p.scl);


  }

  p.draw = function() {
    p.frameRate(10);
    p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
    p.background(0);
    p.stroke(255);
    p.strokeWeight(1);
    p.noFill();

    p.translate(-100, - 100);
    p.rotateX(p.PI/3)

    for (var y = 0; y < p.rows; y++) {
        p.beginShape(p.TRIANGLE_STRIP);
      for (var x = 0; x < p.cols; x++) {
        p.vertex(x * p.scl, y * p.scl, p.random(-10, 10));
        p.vertex(x * p.scl, (y + 1) * p.scl, p.random(-10, 10));
      }
      p.endShape(p.CLOSE);

    }


  }

};

window.designCanvas = new p5(window.design, document.getElementById('canvas'));
