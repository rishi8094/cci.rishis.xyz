var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.design = function(p) {
  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height);
  }

  p.draw = function() {
    p.background("#AB8D59");
    p.strokeWeight(0);

    p.push();
    p.fill("#E0CCAB")
    p.rect((Math.floor(p.width / 15) * 2), 0, Math.floor(p.width / 15), p.height)
    p.fill("#E0CCAB")
    p.rect((Math.floor(p.width / 15) * 4), 0, Math.floor(p.width / 15), p.height)
    p.fill("#E0CCAB")
    p.rect((Math.floor(p.width / 15) * 6), 0, Math.floor(p.width / 15), p.height)
    p.pop();

    p.push();
    p.fill("#6C685C");
    p.rect(0, (Math.floor(p.height / 15) * 2), p.width, Math.floor(p.height / 15))
    p.fill("#6C685C");
    p.rect(0, (Math.floor(p.height / 15) * 4), p.width, Math.floor(p.height / 15))
    p.fill("#6C685C");
    p.rect(0, (Math.floor(p.height / 15) * 6), p.width, Math.floor(p.height / 15))
    p.fill("#6C685C");
    p.rect(0, (Math.floor(p.height / 15) * 8), p.width, Math.floor(p.height / 15))
    p.pop();


    p.push();
    p.fill("#242625");
    p.rect(Math.floor(p.width / 15), 0, Math.floor(p.width / 15), p.height);
    p.fill("#242625");
    p.rect((Math.floor(p.width / 15) * 3), 0, Math.floor(p.width / 15), p.height);
    p.fill("#242625")
    p.rect((Math.floor(p.width / 15) * 5), 0, Math.floor(p.width / 15), p.height);
    p.fill("#242625")
    p.rect((Math.floor(p.width / 15) * 7), 0, Math.floor(p.width / 15), p.height);

    p.pop();

    p.push();
    p.fill("#CB5E49")
    p.rect(p.width - (Math.floor(p.width / 15) * 3), 0, Math.floor(p.width / 40), p.height)
    p.pop();




  }
};

function randomColor() {
  var arr = ["#6284FF", "#52ACFF", "#FAACA8", "#DDD6F3", "#21D4FD", "#B721FF", "#08AEEA", "#2AF598", "#8EC5FC", "#E0C3FC", "#D9AFD9", "#97D9E1", "#0093E9", "#80D0C7", "#74EBD5", "#9FACE6", "#2BD2FF", "#2BFF88"]
  return arr[getRandomInt(0, arr.length - 1)]
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
window.designCanvas = new p5(window.design, document.getElementById('canvas'));
