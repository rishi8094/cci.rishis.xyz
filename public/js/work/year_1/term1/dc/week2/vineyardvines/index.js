var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;

window.design = function(p) {
  p.setup = function() {
    p.frameRate(15);
    p.createCanvas(width, height);
    document.getElementById('canvas').style.height = `${p.height}px`;
    document.getElementById('canvas').style.width = `${p.width}px`;
  }

  p.draw = function() {
      p.resizeCanvas(document.querySelector("#canvas").clientWidth, document.querySelector("#canvas").clientHeight);
    p.background("#fff")

  //Settings
  var config = {
    borderColor: "#000000", //This will change the border
    primaryColor: "#3971F6", // This will change the main body colour
    secondaryColor: "#89D0FB", // This will change the inner outline
    waterColor: "#6DE5FC", // This will change the watersplash colour
    belly: "#ffffff",
    eye: {
      bg: "#ffffff",
      pupil: "#000000"
    },
    startX: 30,
    startY: 40,
    padding: 20,
    pad: {
      x: 40,
      y: 40
    },
    randomColor: true,
    animate: {
      random: false,
      size: true,
      sound: false //Inactive
    }
  }

  //Global
  p.strokeWeight(0);
  if (config.animate.size) {
    p.frameRate(2)
    config.padding = getRandomInt(5, 20);
  }


  var whaleWidth = (19 * config.padding) + config.pad.x;
  var whaleHeight = (12 * config.padding) + config.pad.y;

  var maxVolX = Math.floor(width / whaleWidth);
  var maxVolY = Math.floor(height / whaleHeight);

  for (var y = 0; y < maxVolY; y++) {
    var startY = config.startY + (y * whaleHeight)
    for (var x = 0; x < maxVolX; x++) {
      if (config.randomColor) {
        config.primaryColor = randomColor();
        config.secondaryColor = randomColor();
        config.belly = randomColor();
      }


      if (config.animate.random) {
        p.frameRate(30);
        config.padding = getRandomInt(5, 20);
        whaleWidth = (19 * config.padding) + config.pad.x;
        whaleHeight = (12 * config.padding) + config.pad.y;

        maxVolX = Math.floor(width / whaleWidth);
        maxVolY = Math.floor(height / whaleHeight);
      }


      var startX = config.startX + (x * whaleWidth)

      //Outer
      p.fill(config.borderColor)
      p.stroke(config.borderColor)
      p.square(startX, startY, config.padding);
      p.square(startX + (1 * config.padding), startY - (1 * config.padding), config.padding);
      for (var a = 0; a < 6; a++) {
        p.square(startX + (1 * config.padding) + (a * (1 * config.padding)), startY - (1 * config.padding), config.padding);
      }
      p.square(startX + (7 * config.padding), startY, config.padding);
      p.square(startX + (8 * config.padding), startY, config.padding);
      p.square(startX + (9 * config.padding), startY + (1 * config.padding), config.padding);

      for (var a = 0; a < 4; a++) {
        p.square(startX + (10 * config.padding), startY + (2 * config.padding) + (a * (1 * config.padding)), config.padding);
      }

      p.square(startX + (11 * config.padding), startY + (6 * config.padding), config.padding);
      p.square(startX + (12 * config.padding), startY + (7 * config.padding), config.padding);
      p.square(startX + (13 * config.padding), startY + (7 * config.padding), config.padding);
      p.square(startX + (14 * config.padding), startY + (6 * config.padding), config.padding);
      p.square(startX + (13 * config.padding), startY + (5 * config.padding), config.padding);
      p.square(startX + (13 * config.padding), startY + (4 * config.padding), config.padding);
      p.square(startX + (13 * config.padding), startY + (3 * config.padding), config.padding);
      p.square(startX + (14 * config.padding), startY + (2 * config.padding), config.padding);
      p.square(startX + (15 * config.padding), startY + (3 * config.padding), config.padding);
      p.square(startX + (16 * config.padding), startY + (2 * config.padding), config.padding);
      p.square(startX + (17 * config.padding), startY + (3 * config.padding), config.padding);
      p.square(startX + (17 * config.padding), startY + (4 * config.padding), config.padding);
      p.square(startX + (17 * config.padding), startY + (5 * config.padding), config.padding);
      p.square(startX + (16 * config.padding), startY + (6 * config.padding), config.padding);
      p.square(startX + (16 * config.padding), startY + (7 * config.padding), config.padding);
      p.square(startX + (15 * config.padding), startY + (8 * config.padding), config.padding);
      for (var a = 0; a < 14; a++) {
        p.square(startX + (15 * config.padding) - (11 * (1 * config.padding)), startY + (8 * config.padding), config.padding);
        p.square(startX + (14 * config.padding) - (11 * (1 * config.padding)), startY + (10 * config.padding), config.padding);
        p.square(startX + (13 * config.padding) - (11 * (1 * config.padding)), startY + (10 * config.padding), config.padding);
        p.square(startX + (17 * config.padding) - (11 * (1 * config.padding)), startY + (10 * config.padding), config.padding);
        p.square(startX + (18 * config.padding) - (11 * (1 * config.padding)), startY + (10 * config.padding), config.padding);
        p.square(startX + (14 * config.padding) - (a * (1 * config.padding)), startY + (9 * config.padding), config.padding);
      }
      p.square(startX + (11 * config.padding) - (11 * (1 * config.padding)), startY + (8 * config.padding), config.padding);
      for (var a = 0; a < 7; a++) {
        p.square(startX - (1 * config.padding), startY + (7 * config.padding) - (a * (1 * config.padding)), config.padding);
      }

      //Inner
      p.fill(config.secondaryColor)
      p.stroke(config.secondaryColor)
      for (var a = 0; a < 6; a++) {
        p.square(startX, startY + (6 * config.padding) - (a * config.padding), config.padding);
      }
      for (var a = 0; a < 6; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY, config.padding);
      }
      for (var a = 0; a < 2; a++) {
        p.square(startX + (7 * config.padding) + (a * config.padding), startY + (1 * config.padding), config.padding);
      }
      p.square(startX + (9 * config.padding), startY + (2 * config.padding), config.padding);
      p.square(startX + (14 * config.padding), startY + (7 * config.padding), config.padding);
      p.square(startX + (15 * config.padding), startY + (7 * config.padding), config.padding);
      for (var a = 0; a < 4; a++) {
        p.square(startX + (15 * config.padding), startY + (7 * config.padding) - (a * config.padding), config.padding);
      }
      for (var a = 0; a < 3; a++) {
        p.square(startX + (14 * config.padding), startY + (5 * config.padding) - (a * config.padding), config.padding);
      }
      for (var a = 0; a < 3; a++) {
        p.square(startX + (16 * config.padding), startY + (5 * config.padding) - (a * config.padding), config.padding);
      }

      //water
      p.fill(config.waterColor)
      p.stroke(config.waterColor)
      p.square(startX + (17 * config.padding) - (11 * (1 * config.padding)), startY + (8 * config.padding), config.padding);
      p.square(startX + (17 * config.padding) - (11 * (1 * config.padding)), startY + (9 * config.padding), config.padding);

      //Belly
      p.fill(config.belly);
      p.stroke(config.belly);
      p.square(startX, startY + (7 * config.padding), config.padding);
      for (var a = 0; a < 3; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (7 * config.padding), config.padding);
      }
      for (var a = 0; a < 3; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (8 * config.padding), config.padding);
      }
      p.square(startX + (7 * config.padding), startY + (8 * config.padding), config.padding);
      p.square(startX + (8 * config.padding), startY + (8 * config.padding), config.padding);


      //Eye
      p.fill(config.eye.bg);
      p.stroke(config.eye.bg);
      p.square(startX + (3 * config.padding), startY + (5 * config.padding), config.padding);
      p.square(startX + (4 * config.padding), startY + (5 * config.padding), config.padding);
      p.square(startX + (3 * config.padding), startY + (4 * config.padding), config.padding);
      p.square(startX + (4 * config.padding), startY + (4 * config.padding), config.padding);
      p.fill(config.eye.pupil);
      p.stroke(config.eye.pupil);
      p.square(startX + (3 * config.padding), startY + (5 * config.padding), config.padding);

      //Body
      p.fill(config.primaryColor);
      p.stroke(config.primaryColor);
      for (var a = 0; a < 6; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (1 * config.padding), config.padding);
      }
      for (var a = 0; a < 8; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (2 * config.padding), config.padding);
      }
      for (var a = 0; a < 9; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (3 * config.padding), config.padding);
      }
      for (var a = 0; a < 2; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (4 * config.padding), config.padding);
      }
      for (var a = 0; a < 5; a++) {
        p.square(startX + (5 * config.padding) + (a * config.padding), startY + (4 * config.padding), config.padding);
      }
      for (var a = 0; a < 2; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (5 * config.padding), config.padding);
      }
      for (var a = 0; a < 5; a++) {
        p.square(startX + (5 * config.padding) + (a * config.padding), startY + (5 * config.padding), config.padding);
      }
      for (var a = 0; a < 10; a++) {
        p.square(startX + (1 * config.padding) + (a * config.padding), startY + (6 * config.padding), config.padding);
      }

      for (var a = 0; a < 8; a++) {
        p.square(startX + (4 * config.padding) + (a * config.padding), startY + (7 * config.padding), config.padding);
      }
      p.square(startX + (5 * config.padding), startY + (8 * config.padding), config.padding);
      for (var a = 0; a < 6; a++) {
        p.square(startX + (9 * config.padding) + (a * config.padding), startY + (8 * config.padding), config.padding);
      }
    }
  }
  }
};

function randomColor() {
  var arr = ["#6284FF", "#52ACFF", "#FAACA8", "#DDD6F3", "#21D4FD", "#B721FF", "#08AEEA", "#2AF598", "#8EC5FC", "#E0C3FC", "#D9AFD9", "#97D9E1", "#0093E9", "#80D0C7", "#74EBD5", "#9FACE6", "#2BD2FF", "#2BFF88"]
  return arr[getRandomInt(0, arr.length - 1)]
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
window.designCanvas = new p5(window.design, document.getElementById('canvas'));
