var width = document.querySelector("#canvas").clientWidth;
var height = document.querySelector("#canvas").clientHeight;
window.videoLive = async function(p) {
  p.canvas;
  p.video;

  let model = null;

  p.setup = async function() {
    p.frameRate(15);
    p.canvas = p.createCanvas(width, height);
    p.video = p.createCapture(p.VIDEO);
    p.video.size(1, 1);
    p.video.hide();
    document.getElementById('canvas').style.height = `${height}px`;
    document.getElementById('canvas').style.width = `${width}px`;
    console.log("***---LOADING MODELS---***");
    model = await cocoSsd.load();
    console.log("✅LOADED MODELS");
  }


  p.draw = async function() {
    p.image(p.video, 0, 0, width, height);
    var vidSrc = document.querySelector("video");
    if (model != null) {
      console.log("RUNNING DETECTION MODELS");
      let predictions = await model.detect(vidSrc);
      p.strokeWeight(4);
      p.stroke("blue");
      p.fill(0,0,0,0)
      predictions.forEach((prediction) => {
        //console.log(`🤖FOUND A ${prediction.class} at x: ${prediction.bbox[0]}, y: ${prediction.bbox[1]}, width: ${prediction.bbox[2]-prediction.bbox[0]}, height: ${prediction.bbox[3]-prediction.bbox[1]}`);
        var x1 = ((prediction.bbox[0]/document.getElementById('defaultCanvas0').width) * width);
        var y1 = ((prediction.bbox[1]/document.getElementById('defaultCanvas0').height) * height);
        var x2 = ((prediction.bbox[2]/document.getElementById('defaultCanvas0').width) * width);
        var y2 = ((prediction.bbox[3]/document.getElementById('defaultCanvas0').height) * height);
        console.log(`🤖FOUND A ${prediction.class} at x: ${x1}, y: ${y1}, width: ${Math.abs(x2 - x1)}, height: ${Math.abs(y2 - y1)}`);

        p.rect(x1+30, y1 + 30, x2, y2);
        p.strokeWeight(1);
        p.stroke("red");
        p.textSize(width/30);
        p.text(prediction.class, x1+35, y1 + 45);
      });
    }

  }


};

window.videoLive = new p5(window.videoLive, document.getElementById('canvas'));
