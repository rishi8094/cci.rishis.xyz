<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, viewport-fit=cover" name="viewport">
    <title>Rishi's CCI Portfolio</title>
    <link rel="stylesheet" href="https://latex.now.sh/style.css">
    <script id="MathJax-script" async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  </head>

  <body class="whitepaper">
    <!-- TITLE META -->
    <h1>Experimental Human Computer Interaction</h1>
    <p class="author">Rishi Serumadar<br>February 1, 2021</p>

    <!-- ABSTRACT -->
    <div class="abstract">
      <h2>Abstract</h2>
      <p>
        A simplistic version of modern marketplaces using an invite-only approach that would allow select merchants to target loyal customers to select a range of limited edition products and merchandise. The service will onboard merchants selectively to ensure that platform saturation doesn't occur. In e-commerce, customers often face the "paradox of choice", too many options causes confusion and friction for merchants. The proposed solution will force the merchant to release a handful of products via the store and share a unique link to their core customers. Customers of other brands will therefore be unaware of all the sales occurring via the site. The products will be available for only a few days or until sold out whichever is first and will disappear from the store forever. This offers consumers the privilege to make a gut instinct decision.
      </p>
    </div>

    <!-- INTRODUCTION -->
    <h2>1. Introduction</h2>
    <p>
      Commerce on the Internet is immensely saturated and provides the consumer with absurd amounts of choice, reducing clarity and decision making ability on purchases. An article published, "Journal of Computer-Mediated Communication, Volume 6, Issue 3" reflects the impulsive and often irrational behaviour of unsupervised spending. The concept explored is how "impulse purchases are responses to novelty or escape that break a normal buying pattern." Consumers enjoy the escape and the thrill of shopping. The act itself of checking out is of greater desire than actually obtaining the product and using it. As the world of e-commerce grows and companies find new means of targeting individuals through ads and click funnels. The entire e-commerce process becomes polluted and excessive choice creates elastic demand for consumers having to chase consumers through tools like abandoned cart tracking. These methods are expensive, both mentally for the consumer and financially for the retailer. The store is designed to solve this issue by limiting choice and forcing consumers to choose either yes/no versus a selection of 50 other variants. The checkout process is streamlined from add to cart straight to checkout consumers are greeted by a single checkout page: no ads, or up-selling of other products. Such a simplistic checkout process may appear to reduce profitability for retailers. Still, it may be more beneficial as consumers once committed are rushed to checkout and given little choice to exit at that point.
    </p>

    <!-- DESIGN  -->
    <h2>2. Design</h2>
    <!-- PLANNING -->
    <div>
        <h3>2.1 Planning</h3>
        <figure>
          <img src="./images/design/planning_map.jpg" loading="lazy" width="600" height="500">
          <figcaption>
            Mind Map
          </figcaption>
        </figure>
    </div>

    <!-- INITIAL DESIGN CONCEPT -->
    <div>
      <h3>2.2 Initial Design Concept</h3>
      <p>The design should be minimalistic and simple to follow, no clutter and unobtrusive.</p>
      <br/>
      <figure>
        <img src="./images/design/sketch.png" loading="lazy" width="600" height="400">
        <figcaption>
          Digital Sketch of initial structure
        </figcaption>
      </figure>
      <p>The initial concept utilises a simple splash page concept for when no releases are occurring. When a release occurs, you will be redirected (based on the correct secret link) to the store page where you can view the catalogue available without price. The absence of price should guide people to find products they want and explore the available range. There is no filtering or advanced search options to force consumers to look through products and stop retailers from cluttering the page.</p>
    </div>

    <!-- MOCKUPS -->
    <div>
      <h3>2.3 Mockups</h3>
      <p>All the designs were created using the sketch app.</p>
      <figure>
        <img src="./images/design/mockup/sketch.png" loading="lazy" width="600" height="400">
      </figure>
      <!-- MOCKUP: HOME -->
      <h4>2.3.1 Home</h4>
      <figure>
        <img src="./images/design/mockup/home.png" loading="lazy" width="600" height="400">
      </figure>
      
      <!-- MOCKUP: STORE FRONT -->
      <h4>2.3.2 Store Front</h4>
      <figure>
        <img src="./images/design/mockup/storefront.png" loading="lazy" width="600" height="400">
      </figure>

      <!-- MOCKUP: STORE PRODUCT -->
      <h4>2.3.3 Store Product</h4>
      <figure>
        <img src="./images/design/mockup/storeproduct.png" loading="lazy" width="600" height="400">
      </figure>

      <!-- MOCKUP: STORE CHECKOUT -->
      <h4>2.3.4 Store Checkout</h4>
      <figure>
        <img src="./images/design/mockup/storecheckout.png" loading="lazy" width="600" height="400">
      </figure>

      <!-- MOCKUP: STORE CHECKOUT (ERROR) -->
      <h4>2.3.5 Store Checkout (Error)</h4>
      <figure>
        <img src="./images/design/mockup/storecheckout-error.png" loading="lazy" width="600" height="400">
      </figure>

      <!-- MOCKUP: STORE CHECKOUT (LOADING) -->
      <h4>2.3.6 Store Checkout (Loading)</h4>
      <figure>
        <img src="./images/design/mockup/storecheckout-loading.png" loading="lazy" width="600" height="400">
      </figure>

      <!-- MOCKUP: STORE CHECKOUT (COMPLETE) -->
      <h4>2.3.7 Store Checkout (Complete)</h4>
      <figure>
        <img src="./images/design/mockup/storecheckout-complete.png" loading="lazy" width="600" height="400">
      </figure>
    </div>

  <!-- DEVELOPMENT  -->
  <h2>3. Development</h2>
  <p>The project will be designed to run as a next.js app. Next.js provides many benefits such as Image Cache Support, Static Regeneration and Typescript support.</p>
  <p>The application will use react components for the front end to render content and handle user interaction. The backend will utilise next.js native micro HTTP functionality to provide an internal API written in TypeScript to communicate. The database of choice for this prototype will be PostgreSQL due to highly relational data and performance benefits.</p>
  <!-- DEPENDENCIES -->
  <div>
    <h3>3.1 Dependencies</h3>
    <figure>
      <img src="./images/dev/package-json.png" loading="lazy" width="600" height="500">
      <figcaption>
        Package.json
      </figcaption>
    </figure>
    <div class="definition">
      <b>@prisma</b> - An ORM which can be used to interact with the PostgreSQL database to avoid writing sequel statements.
    </div>
    <div class="definition">
      <b>@sentry</b> - An error/bug reporting tool which can be used to catch errors in production so fixes can be easily deployed without user reporting.
    </div>
    <div class="definition">
      <b>@quirrel/</b> - A Job/Queue management system which will be used to hold the recieved orders in a queue to avoid overselling and limit the effects of a race condition.
    </div>
    <div class="definition">
      <b>@stripe</b> - A payment gateway system which can be used to take payments for sales and route them to the corresponding merchant.
    </div>
    <div class="definition">
      <b>@next-iron-session</b> - A session management tool for Next.js to ensure the integrity of session data and track/store key user events.
    </div>
    <div class="definition">
      <b>swr</b> - A React Hooks library which can be used to fetch data from the API.
    </div>
    <div class="definition">
      <b>validatorjs</b> - A schema-style validation tool to ensure that user inputs meet the correct standard as required.
    </div>
    <!-- REACT COMPONENTS -->
    <div>
      <h3>3.2 React Components</h3>
      <figure>
        <img src="./images/dev/react-blocks.png" loading="lazy" width="600" height="500">
        <figcaption>Left: React Store Front Component, Right: React Store Front Product Sub Component</figcaption>
        <img src="./images/dev/code/storefront.png" loading="lazy" width="600" height="500">
        <figcaption>React Store Front Component</figcaption>
        <img src="./images/dev/code/storefront-product.png" loading="lazy" width="600" height="500">
        <figcaption>React Store Front Product Sub Component</figcaption>
      </figure>
    </div>

    <!-- API -->
    <div>
      <h3>3.3 API Routing</h3>
      <figure>
        <img src="./images/dev/api.jpg" loading="lazy" width="600" height="500">
      </figure>
    </div>

    <!-- PAYMENT -->
    <div>
      <h3>3.3 Payment Gateway</h3>
      <p></p>
      <figure>
        <img src="./images/dev/payment.png" loading="lazy" width="600" height="500">
      </figure>
      <p>The payment logic for a marketplace is different from that of a standard retailer. The payment taken from a consumer should be processed by the platform to ensure the merchant received the money.  However, there are a lot of implications with the platform holding the money. For this reason, I have chosen to use Stripe's Connect API, which can allow me to safely onboard money, choose my routing flow for payments and still provide the merchant with a transparent payment data.</p>
    </div>

    <!-- ORDER QUEUING -->
    <div>
      <h3>3.3 Order Queuing</h3>
      <p>In e-commerce, when many orders are placed in quick succession, there is likely to be a collision better known as a race condition where overallocation of stock can occur. For example, there is one item left, and two people both submit the order simultaneously,  both buyers will be allocated the same unit.

        A common solution would be to utilise "ACID TRANSACTIONS", the issue with this is that stripe doesn't natively support such logic so there isn't a way to ensure payment is taken and stock is allocated.
        
        For this, I devised an approach to utilise a queuing system. All orders would be sent to a queue a FIFO (First in First Out), this would create an order to the queue as one order would always be sent into the queue before another. The issue was for next.js no such solution existed natively.</p>
      <figure>
        <img src="./images/dev/queue.png" loading="lazy" width="600" height="500">
        <figcaption>copyright ©  2020 <a href="https://quirrel.dev/">Quirrel</a> c/o Simon Knott </figcaption>
      </figure>

      <p>To solve this, I reached out to a Developer of an Open Source Library that was being developed. We jumped on a few calls where I explained the process and use-case to him. We were able to create this functionality into his Library and implement the queueing logic.</p>
      <figure>
        <img src="./images/dev/simonemail.png" loading="lazy" width="600" height="500">
      </figure>
    </div>
  </div>

  <pre></pre>
  <!-- FINAL  -->
  <h2>4. Final Product</h2>
  <div>
    <figure>
      <img src="./images/final/home.png" loading="lazy" width="600" height="500">
      <figcaption>Home Splash Page</figcaption>
    </figure>
    <figure>
      <img src="./images/final/store.png" loading="lazy" width="600" height="500">
      <figcaption>Merchant Store Page</figcaption>
    </figure>
    <figure>
      <img src="./images/final/product.png" loading="lazy" width="600" height="500">
      <figcaption>Merchant Product Page</figcaption>
    </figure>
    <figure>
      <img src="./images/final/checkout.png" loading="lazy" width="600" height="500">
      <figcaption>Store Checkout Page</figcaption>
    </figure>
  </div>
  
  <pre></pre>
  <!-- DEMO  -->
  <h2>5. Demo</h2>
  <a href="https://store-staging.nospace.io">HOME PAGE (https://store-staging.nospace.io)</a>
  <a href="https://store-staging.nospace.io/releases/rishi">MERCHANT STORE FRONT (https://store-staging.nospace.io/releases/rishi)</a>
  <div>
    <h3>5.1 Checkout</h3>
    <p>You can test the live order queueing/checkout functionality by pressing purchase on a product then filling out random details and providing the following payment details:</p>
    <p>
      <b>CARD NUMBER:</b> <i>4242 4242 4242 4242</i>
      <br>
      <b>CARD EXP:</b> <i>12/29</i>
      <br>
      <b>CVV:</b> <i>123</i>
    </p>
  </div>

  </body>
</html>
