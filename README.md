## Viewing
This repository is a mirror of the content located at https://cci.rishis.xyz. To view the content of this site locally you may download and open the `index.html` file located in the build folder.

### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```
